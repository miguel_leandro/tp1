/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Página principal",url:"index.html"},
{text:"Páginas relacionadas",url:"pages.html"},
{text:"Pacotes",url:"namespaces.html",children:[
{text:"Pacotes",url:"namespaces.html"},
{text:"Funções do Pacote",url:"namespacemembers.html",children:[
{text:"Tudo",url:"namespacemembers.html",children:[
{text:"a",url:"namespacemembers.html#index_a"},
{text:"b",url:"namespacemembers.html#index_b"},
{text:"c",url:"namespacemembers.html#index_c"},
{text:"d",url:"namespacemembers.html#index_d"},
{text:"e",url:"namespacemembers.html#index_e"},
{text:"f",url:"namespacemembers.html#index_f"},
{text:"i",url:"namespacemembers.html#index_i"},
{text:"j",url:"namespacemembers.html#index_j"},
{text:"l",url:"namespacemembers.html#index_l"},
{text:"m",url:"namespacemembers.html#index_m"},
{text:"n",url:"namespacemembers.html#index_n"},
{text:"p",url:"namespacemembers.html#index_p"},
{text:"q",url:"namespacemembers.html#index_q"},
{text:"r",url:"namespacemembers.html#index_r"},
{text:"s",url:"namespacemembers.html#index_s"},
{text:"t",url:"namespacemembers.html#index_t"},
{text:"v",url:"namespacemembers.html#index_v"},
{text:"x",url:"namespacemembers.html#index_x"},
{text:"y",url:"namespacemembers.html#index_y"}]},
{text:"Funções",url:"namespacemembers_func.html"},
{text:"Variáveis",url:"namespacemembers_vars.html",children:[
{text:"a",url:"namespacemembers_vars.html#index_a"},
{text:"b",url:"namespacemembers_vars.html#index_b"},
{text:"c",url:"namespacemembers_vars.html#index_c"},
{text:"d",url:"namespacemembers_vars.html#index_d"},
{text:"e",url:"namespacemembers_vars.html#index_e"},
{text:"f",url:"namespacemembers_vars.html#index_f"},
{text:"i",url:"namespacemembers_vars.html#index_i"},
{text:"j",url:"namespacemembers_vars.html#index_j"},
{text:"l",url:"namespacemembers_vars.html#index_l"},
{text:"m",url:"namespacemembers_vars.html#index_m"},
{text:"n",url:"namespacemembers_vars.html#index_n"},
{text:"p",url:"namespacemembers_vars.html#index_p"},
{text:"q",url:"namespacemembers_vars.html#index_q"},
{text:"r",url:"namespacemembers_vars.html#index_r"},
{text:"s",url:"namespacemembers_vars.html#index_s"},
{text:"t",url:"namespacemembers_vars.html#index_t"},
{text:"v",url:"namespacemembers_vars.html#index_v"},
{text:"x",url:"namespacemembers_vars.html#index_x"},
{text:"y",url:"namespacemembers_vars.html#index_y"}]}]}]},
{text:"Ficheiros",url:"files.html",children:[
{text:"Lista de ficheiros",url:"files.html"}]}]}
