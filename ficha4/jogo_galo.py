##
# \brief Jogo do Galo em Python
# \details Jogo do Galo recriado em linguagem Python, mostra quem ganhou e se empatou
# \author Miguel Frois
# \date 16 dez 2019
# \bug Sem Erros detetados
# \version 1.0
# \copyright GNU Public License.
...
## Array do Campo de jogo
a = ['_', '_', '_',
     '_', '_', '_',
     '_', '_', '_']
...
...
## Função para verificar qual dos jogadores ganhou
#
# \param jogador : variavel char (X ou O)
# \param nJogador : Qual o Jogador que está a verificar (1 ou 2)
# \return Verdadeiro caso o jogador ganhou ou Falso caso nenhuma das condições estejam presentes
def verifi_ganhou(jogador,nJogador):
     if a[0] == a[1] and a[0] == a[2] and a[0] == jogador:
         print("\nJogador",nJogador, "Ganhou")
         return True

     if a[3] == a[4] and a[3] == a[5] and a[3] == jogador:
         print("\nJogador", nJogador, "Ganhou")
         return True

     if a[6] == a[7] and a[6] == a[8] and a[6] == jogador:
         print("\nJogador", nJogador, "Ganhou")
         return True

     if a[0] == a[3] and a[0] == a[6] and a[0] == jogador:
         print("\nJogador", nJogador, "Ganhou")
         return True

     if a[1] == a[4] and a[1] == a[7] and a[1] == jogador:
         print("\nJogador", nJogador, "Ganhou")
         return True

     if a[2] == a[5] and a[2] == a[8] and a[2] == jogador:
         print("\nJogador", nJogador, "Ganhou")
         return True

     if a[0] == a[4] and a[0] == a[8] and a[0] == jogador:
         print("\nJogador", nJogador, "Ganhou")
         return True

     if a[2] == a[4] and a[2] == a[6] and a[2] == jogador:
         print("\nJogador", nJogador, "Ganhou")
         return True

     return False
...




print("Jogador 1: X")
print("Jogador 2: O")
...
## Posicão onde jogar
posisao = 0
## Caracter do Jogador 1, neste caso, é "X"
jogador1 = 'X'
## Caracter do Jogador 2, neste caso, é "O"
jogador2 = 'O'
## Qual dos jogadores tem de jugar
nJogador = 1
## Declara jogador como jogador1 para jogar primeiro
jogador = jogador1
## Contador do número de jogadas, onde o limite é 9 jogados no total
contadorJogadas = 0
...

while True:

    print("", a[0], a[1], a[2], "\n", a[3], a[4], a[5],"\n", a[6], a[7], a[8])
    print ("Jogador", nJogador, end='')
    posisao = input(" escolha a posição:")

    if (nJogador==1):
        jogador = jogador1
    else:
        jogador = jogador2
    ## Verifica se a posisao que nJogador escolheu é válida ou não
    teste = False

    if posisao == '1' and (a[0] != jogador2 and a[0] != jogador1):
        a[0] = jogador
        teste=True

    if posisao == '2' and (a[1] != jogador2 and a[1] != jogador1):
        a[1] = jogador
        teste = True

    if posisao == '3' and (a[2] != jogador2 and a[2] != jogador1):
        a[2] = jogador
        teste = True

    if posisao == '4' and (a[3] != jogador2 and a[3] != jogador1):
        a[3] = jogador
        teste = True

    if posisao == '5' and (a[4] != jogador2 and a[4] != jogador1):
        a[4] = jogador
        teste = True

    if posisao == '6' and (a[5] != jogador2 and a[5] != jogador1):
        a[5] = jogador
        teste = True

    if posisao == '7' and (a[6] != jogador2 and a[6] != jogador1):
        a[6] = jogador
        teste = True

    if posisao == '8' and (a[7] != jogador2 and a[7] != jogador1):
        a[7] = jogador
        teste = True

    if posisao == '9' and (a[8] != jogador2 and a[8] != jogador1):
        a[8] = jogador
        teste = True


    if (verifi_ganhou(jogador, nJogador)==True):
        break

    if (teste==True):
        contadorJogadas += 1
        if (nJogador==1):
            nJogador = 2
        else:
            nJogador = 1

    if (contadorJogadas>=9):
        print("Empate")
        break

print("", a[0], a[1], a[2], "\n", a[3], a[4], a[5],"\n", a[6], a[7], a[8])