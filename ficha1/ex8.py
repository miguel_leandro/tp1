...
## Mais um exercicio, para se familiarizar com CONDIÇOES e OPERADORES. 
#
# Implemnte um programa para determinar o Máximo de 3 valores!
#

v1 = float(input("Insira o valor1 :  "))
v2 = float(input("Insira o valor2 :  "))
v3 = float(input("Insira o valor3 :  "))
""" Desta vez usando FLOAT, neste caso surgirar casa decimais, insira 3 valores.
	V1 V2 V3;
"""

# Passa direto para a condição que irá comparar com as possivilidade entre 
# parenteses () e auxilio de > MAIOR , < MENOR. 
if (v1 > v2) and (v1 > v3):
	""" Compara V1  com V2 e V3, depois imprime
	"""
    print('O V1 possui maior valor. ')
elif(v2 > v1) and (v2 < v3):
	""" Compara V2 com V1 e V3, depois imprime
	"""
    print('O V2 possui o maior valor.')
else:
	""" Por fim, não e necessário efetuas V3 pela as demais, pois quando acontece a comparaço de 
		V1 e V2, caso nao seja nenhuma delas então de CERTEZA é V3.
	"""
    print(' O V3 possui o maior valor.')

#
## Volto a repetir, exercicios para pratica. 

...
