## Função para converter euros para dolares
# \param euros : valor em euros
# \param taxaconver : valor 1.17
# \sa conversao_invert faz a conversão inversa.
def conversao(euros,taxaconver):
    dolares = euros * taxaconver
    print(dolares)
    return dolares

## Função para converter dolares para euros
# \param dolares : valor em dolares
# \param taxaconver : valor 1.17
# \sa conversao faz a conversão inversa.
def conversao_invert(dolares,taxaconver):
    euros = dolares / taxaconver
    print(euros)

## Varivel float que vai ser inserida pelo utilizador
euros = float(input("Escreva a quantia de euros: "))

## Variavel float, utilizada para converter 'euros' para 'dolares'
taxaconver = 1.17

dolares=conversao(euros,taxaconver)
conversao_invert(dolares,taxaconver)