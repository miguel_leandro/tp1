nota_portuges = int(input("Insira a nota de Português: "))
nota_ingles = int(input("Insira a nota de Inglês: "))
nota_matematica = int(input("Insira a nota de Matemática: "))
nota_geografia = int(input("Insira a nota de Geografia: "))

media = (nota_portuges + nota_ingles + nota_geografia + nota_matematica)/4

if media >= 9.5:
    print("O aluno está Aprovado com media de: ", media)
else:
    print("O aluno está Reprovado com media de: ", media)