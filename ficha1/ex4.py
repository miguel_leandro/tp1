...
## Implemente um programa para determinar perimetro e area de circunferencia.
#
#
raio = float(input(' Introduza o valor do Raio '   ))
""" Deseja que o usuario insir a um valor para se fazer o calculo do Raio.
"""

pi = 3.14
""" Valor do PI
"""

circunferencia = 2 * 3.14 * raio
""" Formula de como calcular uma circunferencia em DEVELOP (PC)
"""

print(" A medida da circunferencia do circulo: {:.2f}".format(circunferencia) + "  de circunferencia")
""" Irá imprimir todo o calculo, {:.2f}.format = fomato de String (Circunferencia)
	+ , assim imprimir dentro de um texto organizado e limpo. Muito usando em Python
"""

## Calculo que consiste em dar radianos de um determinado valor, muito usado em maquinas de calcular.
...
