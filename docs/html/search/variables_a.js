var searchData=
[
  ['n_148',['n',['../namespaceex9.html#a2ea03ddfde01a0d86ad0312303212ad6',1,'ex9']]],
  ['njogador_149',['nJogador',['../namespacejogo__galo.html#a784b9ea07c3ff79e05f5437b5795d235',1,'jogo_galo']]],
  ['nome_150',['nome',['../namespaceex8.html#a0c357d99db2bfaba3fd9d330fbd6117b',1,'ex8']]],
  ['nota_151',['nota',['../namespaceex5.html#a0bc7486a0985ff5bfb1ed777e4e3873b',1,'ex5']]],
  ['nota_5fgeografia_152',['nota_geografia',['../namespaceex4.html#a96aa7c347e74629ebf4e26766be9068f',1,'ex4']]],
  ['nota_5fingles_153',['nota_ingles',['../namespaceex4.html#a132a6db59407a44c6a8d790df80fb621',1,'ex4']]],
  ['nota_5fmatematica_154',['nota_matematica',['../namespaceex4.html#aa48d1f09dd595b49478e370a9d43307a',1,'ex4']]],
  ['nota_5fportuges_155',['nota_portuges',['../namespaceex4.html#accf2703c883f3ef768ec2f978ca9d474',1,'ex4']]],
  ['novo_156',['novo',['../namespaceex8.html#adf19ae8624b598879450be4af1044db2',1,'ex8']]],
  ['num_157',['num',['../namespaceex1.html#a5094475ca81019a2335accfc05e29c92',1,'ex1.num()'],['../namespaceex5.html#a8ae91609e6f2e508edfcf0a71e348408',1,'ex5.num()'],['../namespaceex7.html#a4b1242224177ff0a351216b8f8d24aff',1,'ex7.num()']]]
];
