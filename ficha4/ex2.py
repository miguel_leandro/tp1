...
## Lista que vai guardar 10 variaveis int
lista = []
## Conta a quantidade de números a ser inseridos, como também vai ser usado para guardar um número no Array 'lista'
i = 0
...
while i < 10:
    ## Variavel que guarda um valor para inserir no Array 'lista'
    valor = int(input("Insira um valor no vetor B: "))
    lista.insert(i, valor)
    i = i + 1

print(lista.sort())