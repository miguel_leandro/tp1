...
## Lista que vai guardar 10 variaveis int
lista = []
## Conta a quantidade de números a ser inseridos, como também vai ser usado para guardar um número no Array 'lista'
i = 0
## Conta e Guarda o total de números pares
par = 0
## Conta e Guarda o total de números impares
impar = 0
...

while i < 10:
    ## Variavel que guarda um valor para inserir no Array 'lista'
    valor = int(input("Insira um valor no vetor A: "))
    lista.insert(i,valor)
    resultado = valor % 2

    if resultado == 0:
        par = par + 1
    else:
        impar = impar + 1
    i = i + 1

    print(i)

print("Diferença entre o max e o min da Lista: ", max(lista) - min(lista))
print("Número de pares: ", par)
print("Número de impares: ", impar)
print(lista)
...