# 112 %2 =0 2
# 112/2 =56
#
# 52%2 = 0 2
# 52/2 = 26
#
# 26 %2 =0 2
# 26/2 = 13
#
# 13%2 !=0
# 13%3 !=0
# 13%4 !=0
# 13%# =0

n = int(input("Digite um número inteiro >1: "))

fator = 2

print(int(n), " = ", end='')
while True:
    if (n % fator == 0):
        n = n / fator
        print(fator)
    else:
        fator = fator + 1
    if (n <= 1):
        break;
