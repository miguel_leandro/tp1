# GIT #

Git (/ɡɪt/)[7] is a distributed version-control system for tracking 
changes in source code during software development.[8] 
It is designed for coordinating work among programmers, but it can be 
used to track changes in any set of files. Its goals
include speed,[9] data integrity,[10] and support for distributed,
 non-linear workflows.[11]

Git was created by Linus Torvalds in 2005 for development of the Linux 
kernel, with other kernel developers contributing to its initial
development.[12] Its current maintainer since 2005 is Junio Hamano.
 As with most other distributed version-control systems, and unlike
most client–server systems, every Git directory on every computer is
 a full-fledged repository with complete history and full
 version-tracking abilities, ndependent of network access or a 
 central server.[13]

Git is free and open-source software distributed under the terms of 
the GNU General Public License version 2. 

## Histotia do GIT ##

Git development began in April 2005, after many developers of the Linux
 kernel gave up access to BitKeeper, a proprietary source-control management 
(SCM) system that they had formerly used to maintain the project.[14] 
The copyright holder of BitKeeper, Larry McVoy, had withdrawn free use of the
product after claiming that Andrew Tridgell had created SourcePuller 
by reverse engineering the BitKeeper protocols.[15] The same incident also spurred
the creation of another version-control system, Mercurial. 

Entre outros, o mais ultilizado em nossa curso. Ferramenta esta que
fazemos documentaçao e registros, um importantando para o outro. Bitbucket.

# Bitbucket #

Bitbucket é um serviço de hospedagem de projetos controlados através 
do Mercurial,[2] um sistema de controle de versões distribuído. 
É similar ao GitHub(que utiliza Git, somente). Bitbucket têm um serviço
grátis e um comercial. O serviço é escrito em Python.[3] Num blog de
2008, de Bruce Eckel comparou Bitbucket 
favoravelmente ao sítio web Launchpad,[4] que utiliza Bazaar.

O Bitbucket também suporta repositórios usando o sistema de controle de versões Git. 
