...
## Programa que irá calcular matematicamente a formula do produto. 
#
#
from random import *
""" Irá importar tudo que existe na biblioteca para ajudar em qualquer 
que seja o calculo resolvente
"""
a = (uniform(0,10))
b = (uniform(5,20))
c = (uniform(5,20))

d = (b*b - 4.0*a*c)
""" Irá fazer o calculo do produto
"""
if (d > 0):
    """Condição que irá auxiliar no calculo da raiz quadrada e saida do calculo
    """
    rq = (d ** 0.5)
    x1 = (-b + rq) / 2.0 * a
    x2 = (-b - rq) / 2.0 * a

    print(x1)
    print(x2)
    """Irá imprimir o X1 e o X2 encontrado no calculo realizado, após
     passar por todas as etapas, caso sejam verdadeiras
    """
else:
    print ('não possui raiz Quadrada! ')
    """Condição caso não possua uma raiz quadrada inteira
    """


...
