...
## Variavel float inserida pelo utilizador
raio = float(input("Escreva o valor do raio da circunferência: "))
## Variavel float
pi = 3.14
## Variavel float da conta: 'pi' a multiplicar for 'raio' elevado a 2
areacircun = pi * raio**2
...
print("\nA área do círculo é: ",areacircun)

## Variavel float da conta: 2 a multiplicar por 'pi' a multiplicar por 'raio'
peri = (2*pi * raio)

print("O perímetro do círculo é:  ",peri)