...
### EX5
# Implente um programa que verifica se o aluno é aprovado.
#
prova1 = int(input("Insira a nota do 1º Semestre :   "))
prova2 = int(input("Insira a nota do 2º Semestre :   "))
""" Declara e solicita a inserção de variaveis através do teclado, neste caso 
	Prova1 e Prova2
"""

media = (prova1 + prova2) / 2
""" Irá calcular a média -> Declara a mia e com o sinal de '=' faz o calculo,
	chamando as variaveis
"""

print(media)
""" Irá imprimir a média, atenção apenas números inteiros para não complicar
"""

if (media >= 9.5):
	""" Condição que irá mostra se o aluno foi aprovado ou não
	"""
    print("Aluno aprovado no ano letivo.")
else:
	"""Se não foi aprovad irá imprimir que ele não foi aprovado no ano letivo 
	"""
    print("Aluno não aprovado neste ano letivo.  "  +  " Consulte calendários de provas de recuperação! ")

#
## Calculo médio, bom para praticar a memorização de condições IF, ElSE   
...
